<?php
// Template Name: Página Lista de Lobinhos
?>

<?php get_header(); ?>

  <main>
    <section class="example">
      <div class="container">

        <div class="searchContainer">
          <input class="searchWolf" type="search" name="" id="" placeholder="Digite a busca aqui...">
          <a href="../Adicionar Lobinho/addWolfePage.html" class="btnSearchWolf"> + Lobo </a href="../Adicionar Lobinho/addWolfePage.html"><br>
          
          <input class="checkSearchWolf" type="checkbox" name="" id="">
          <span class="checkText">Ver lobinhos adotados</span>     
        </div>
                
      </div>
    </section>

    <div class="linha1">

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

      <div class="exampleImgContainer">              
        <?php if( get_field('lobo_foto') ): ?>
          <img class="exampleImg" src="<?php the_field('lobo_foto'); ?>" alt="">
        <?php endif; ?>  
      </div>
      
      <div class="exampleInfoContent">
        <div class="exampleNameContainer">
          <h3 class="exampleName">
            <?php the_field('lobo_nome'); ?>
          </h3>
        </div>
        <div class="exampleAgeContainer">
          <p class="exampleAge">
            <?php the_field('lobo_idade'); ?>
          </p>
        </div>
        <div class="exampleTextContainer">
          <p class="exampleText mBt70">
            <?php the_field('lobo_descricao'); ?>
          </p>
        </div>
      </div>
      <?php endwhile; else: ?>
        <p>desculpe, o post não segue os critérios escolhidos</p>  
      <?php endif; ?>
      
      <?php echo paginate_links()?>
    </div>
  </main>

  <!-- Detalhe do footer com linear gradient -->
  <div class="line"> </div>  
  

<?php get_footer(); ?>