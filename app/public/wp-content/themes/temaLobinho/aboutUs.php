<?php
// Template Name: Quem Somos
?>

<?php get_header(); ?>

  <main>
    <p class="title"><?php the_field('quem_somos_titulo'); ?></p>
    <p class="text"> 
    <?php the_field('quem_somos_descricao'); ?>
    </p>
  </main>

  <!-- Detalhe do footer com linear gradient -->
  <div class="line"> </div>
  
<?php get_footer(); ?>