<footer class="footer">

    <div class="container">

      <div class="row gap_footer justify_center justify_between_lg">

        <div class="row justify_center gap_footer">
          <div class="map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3675.1884418257!2d-43.133258399999995!3d-22.9064193!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x99817e444e692b%3A0xfd5e35fb577af2f5!2sUFF%20-%20Instituto%20de%20Computa%C3%A7%C3%A3o!5e0!3m2!1spt-BR!2sbr!4v1658468059927!5m2!1spt-BR!2sbr" width="100%" height="200" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
          </div>

          <div class="infos_footer">

            <ul class="content_footer">
              <li class="content_item">
                <img src="<?php echo get_stylesheet_directory_uri() ?>/imagens/location.svg" alt=""><span class="text_footer">Av. Milton Tavares de Souza, s/n - Sala 115 B <br class="show_md"> - Boa Viagem, Niterói - RJ, 24210-315</span>              
              </li>
              <li class="content_item">
                <img src="<?php echo get_stylesheet_directory_uri() ?>/imagens/tel.svg" alt=""><span class="text_footer">(99) 99999-9999</span>
              </li>
              <li class="content_item">
                <img src="<?php echo get_stylesheet_directory_uri() ?>/imagens/mail.svg" alt=""><span class="text_footer">salve-lobos@lobINhos.com</span>
              </li>   
            </ul>
            <a href="../Quem Somos/aboutUs.html" class="btn_footer">Quem Somos</a>

          </div>
        </div>

        <div class="credit">
          <p class="text_credit">Desenvolvido com </p>
          <img src="<?php echo get_stylesheet_directory_uri() ?>/imagens/paws.png" width="104px" height="104px" alt="">
        </div>

      </div>

    </div>
  </footer>
  <?php wp_footer(); ?>
</body>
    <script src="./scriptHomeGetWolfe.js"></script>
</html>