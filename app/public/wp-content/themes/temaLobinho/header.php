<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Darker+Grotesque:wght@400;700&display=swap" rel="stylesheet">
    <?php wp_head(); ?>
    <title>Home</title>
</head>
<body>
  <header class="navigation_bar">

      <div class="container">
  
        <div class="row justify_between">
          
          <a href="#"><img src="<?php echo get_stylesheet_directory_uri() ?>/imagens/logo.png" class="logo hideLg" width="95px" height="110px" alt=""></a>    
          
          <div class="container_hamburguer wLg100">
            
            <input type="checkbox" id="checkbox-hamburguer">
            
            <label class="hamburguer" for="checkbox-hamburguer">
              <span class="bar"></span>
              <span class="bar"></span>
              <span class="bar"></span>
            </label>
            
            <nav class="menu gap_menu justify_between_lg wLg100 alignCenter">
              <a href="./home.php" class="link">Nossos Lobinhos</a>
              <a href="#"><img  src="<?php echo get_stylesheet_directory_uri() ?>/imagens/logo.png" class="showLg" width="95px" height="110px" alt=""></a>
              <a href="../Quem Somos/aboutUs.html" class="link">Quem Somos</a>
            </nav>

          </div>
          
        </div>
  
      </div>
  
  </header>