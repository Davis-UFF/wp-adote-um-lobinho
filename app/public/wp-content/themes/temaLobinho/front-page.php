<?php
// Template Name: Página Inicial
?>

<?php get_header(); ?>

  <main>

    <section class="header">
      <div class="container">

        <div class="headerContainer">

          <div class="headerTitleContainer">
            <h2 class="headerTitle"><?php the_field('titulo_inicial'); ?></h2>
          </div>

          <div class="headerTextContainer">
            <p class="headerText">
            <?php the_field('descricao_inicial'); ?>
            </p>
          </div>
          
        </div>

      </div>
    </section>

    <section class="about">
      <div class="container">

        <div>

          <div class="aboutTitleContainer">
            <h2 class="aboutTitle"><?php the_field('titulo_sobre'); ?></h2>
          </div>

          <div class="aboutTextContainer">
            <p class="aboutText"> 
            <?php the_field('descricao_sobre'); ?>
            </p>
          </div>

        </div>

      </div>
    </section>

    <section class="values">
      <div class="container">          

        <div class="valuesTitleContainer">
          <h2 class="valuesTitle"><?php the_field('valores_titulo'); ?></h2>
        </div>

        <div class="valuesItems row rowNoWrapLg justify_center gap30">

          <div class="item">

            <div>
              <img class="subItemImg marginBtLg" src="<?php echo get_stylesheet_directory_uri() ?>/imagens/insurance.png" alt="">
            </div>
              <h3 class="subItemTitle marginBtLg">
                <?php the_field('valores_subtitulo_1'); ?>
              </h3>
              <p class="subItemText">
                <?php the_field('valores_descricao_1'); ?>
              </p>

          </div>

          <div class="item">

            <div>
              <img class="subItemImg marginBtLg" src="<?php echo get_stylesheet_directory_uri() ?>/imagens/care.png" alt="">
            </div>
              <h3 class="subItemTitle marginBtLg">
                <?php the_field('valores_subtitulo_2'); ?>
              </h3>       
              <p class="subItemText">
                <?php the_field('valores_descricao_2'); ?>
              </p>           

          </div>

          <div class="item">

            <div>
              <img class="subItemImg marginBtLg" src="<?php echo get_stylesheet_directory_uri() ?>/imagens/frame.png" alt="">
            </div>   
              <h3 class="subItemTitle marginBtLg">
              <?php the_field('valores_subtitulo_3'); ?>
              </h3> 
              <p class="subItemText">
              <?php the_field('valores_descricao_3'); ?>
              </p>
          

          </div>

          <div class="item">

            <div>
              <img class="subItemImg marginBtLg" src="<?php echo get_stylesheet_directory_uri() ?>/imagens/rescue.png" alt="">
            </div>
              <h3 class="subItemTitle marginBtLg">
              <?php the_field('valores_subtitulo_4'); ?>
              </h3>
              <p class="subItemText itemText">
              <?php the_field('valores_descricao_4'); ?>
              </p>    

          </div>

        </div>        

      </div>
    </section>

    <section class="example" >
      <div class="container containerExample">

        <div class="exampleTitleContainer">
          <h2 class="exampleTitle">Lobos Exemplo</h2>
        </div>

        <div class="linha1">

          <div class="exampleImgContainer">              
            <?php if( get_field('lobo_foto') ): ?>
              <img class="exampleImg" src="<?php the_field('lobo_foto'); ?>" alt="">
            <?php endif; ?>  
          </div>
          
          <div class="exampleInfoContent">
            <div class="exampleNameContainer">
              <h3 class="exampleName">
                <?php the_field('lobo_nome'); ?>
              </h3>
            </div>
            <div class="exampleAgeContainer">
              <p class="exampleAge">
                <?php the_field('lobo_idade'); ?>
              </p>
            </div>
            <div class="exampleTextContainer">
              <p class="exampleText mBt70">
                <?php the_field('lobo_descricao'); ?>
              </p>
            </div>
          </div>

        </div>      

      </div>
    </section>

  </main>

  <!-- Detalhe do footer com linear gradient -->
  <div class="line"> </div> 

<?php get_footer(); ?>

  