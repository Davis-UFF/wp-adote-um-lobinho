<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'n0zWQz+ZRYWL17AndvBblIQ6Vl+79mtWEbChC85MZ41nn8sZGDAoIf8YPqg8AZrTFBGzmfvl0LewSYtx4uN8Ng==');
define('SECURE_AUTH_KEY',  'fLDIrM1HFLU7gUVK7RRVpX79cVOzF2k+nHDLrmKJR5EG5h4VrCK54zNj6fVHqT2AHk3sc1e779QZbx8o3oS7lQ==');
define('LOGGED_IN_KEY',    'dNlGxl0IryQWgclaE2GYu+MpmrPlFIADHB1Nb0qG0Vr/w1ddvSWBrJG4JRA407goDha8O9YW1lzIg2QDp1FPiQ==');
define('NONCE_KEY',        'LY8FbU/OrPUfGvjT9y8anYl1MtrcN4n1kSD/X7v/LLWiNtAq8EeIg7POvPh1li9Q4b6mhQgJqS6tBnTPcDniqQ==');
define('AUTH_SALT',        'MbImCLIslZXjGIVzr9iQgqy0vpyUWEbGi84bdjy0ytAZYAkkf8QcVOxaLMxQtXD0UgXEvjVu7n8d+ACbZhr+xA==');
define('SECURE_AUTH_SALT', 'c8InKaNHloAAt7bzLr/PQDwfsymGrUQUWWgU6ZPRqAZ+DlCBcfKErJhDIOfSfEZfoBkHezx5CePOFrNOACt0xA==');
define('LOGGED_IN_SALT',   'zAkzvZ0LFtBHpNQJB1uVnGAE4B0cNBP+PnkP/udqA3Mf3EHIakyz5yc0wPkTYlsmhS8xh93v3YbKYG1ceBlriQ==');
define('NONCE_SALT',       'EyAHXMTnFAp81QAdwHH0YQQo1VZJlqrA4uQYwsw7HrpycLCPiVMu+hKLjbZlo3wTt5asSKwQJ4G4CLekoSbAMw==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
